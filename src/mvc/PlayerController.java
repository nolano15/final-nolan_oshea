package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayerController implements ActionListener {
	private Model model;
	private PlayerView view;
	private String flx;
	public int max = 10;

	// command
	public void actionPerformed(ActionEvent e){
		if("Height".equals(e.getActionCommand())) {
			model.setHt(model.getHt() + 1);
			max--;
		}
		else if("Speed".equals(e.getActionCommand())) {
			model.setSpd(model.getSpd() + 1);
			max--;
		}
		else if("Strength".equals(e.getActionCommand())) {
			model.setStr(model.getStr() + 1);
			max--;
		}
		else if(flx.equals(e.getActionCommand())) {
			model.setFlx(model.getFlx() + 1);
			max--;
		}
		if(max == 0)
			view.getFrame().setVisible(false);
	}

	public void addModel(Model m){
		this.model = m;
	} 

	public void addView(PlayerView v){
		this.view = v;
	}
	
	public PlayerController(String flx) {
		this.flx = flx;
	}
} 
