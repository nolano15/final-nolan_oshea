package mvc;

public class PlayerMVC {
	Model model;
	PlayerView view;
	PlayerController controller;
	public PlayerMVC(String position) {
		model = new Model();
		view = new PlayerView(position, 50);

		model.addObserver(view);
		
		controller = null;
		switch(position) {
	        case "QB": 	controller = new PlayerController("Throwing");
	        			break;
	        case "RB": 	controller = new PlayerController("Elusiveness");
	        			break;
	        case "WR":	controller = new PlayerController("Catching");
		}
		controller.addModel(model);
		controller.addView(view);
		view.addController(controller);
	}
	
	public Model getModel() {
		return model;
	}
	
	public PlayerView getView() {
		return view;
	}
	
	public PlayerController getController() {
		return controller;
	}
} 
