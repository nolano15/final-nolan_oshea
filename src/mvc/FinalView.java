package mvc;

import java.awt.Frame;
import java.awt.Label;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

public class FinalView implements Observer {
	public FinalView(int wins, int verticalPosition) {
		Frame frame = new Frame("Result");
		frame.add("North", new Label(Integer.toString(wins)));
		
		frame.addWindowListener(new CloseListener());
		frame.setSize(350, 100);
		frame.setLocation(100, verticalPosition);
		frame.setVisible(true);
	}

	public void update(Observable obs, Object obj) {}

	public static class CloseListener extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			e.getWindow().setVisible(false);
			System.exit(0);
		} 
	} 
} 