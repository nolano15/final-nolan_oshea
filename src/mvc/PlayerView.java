package mvc;

import java.awt.Button;
import java.awt.Panel;
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.WindowEvent; 
import java.awt.event.WindowAdapter; 
import java.util.Observable;
import java.util.Observer;
import java.awt.event.ActionListener; 

public class PlayerView implements Observer {
	public Frame frame;
	private Button buttonHt, buttonSpd, buttonStr, buttonFlx;
	
	public PlayerView(String position, int verticalPosition) {
		frame = new Frame(position);
		frame.add("North", new Label("You have 10 clicks to boost stats"));

		Panel panel = new Panel();
		buttonHt = new Button("Height");
		panel.add(buttonHt);
		buttonSpd = new Button("Speed");
		panel.add(buttonSpd);
		buttonStr = new Button("Strength");
		panel.add(buttonStr);
		
		String flx = null;
		switch(position) {
	        case "QB": 	flx = "Throwing";
	        			break;
	        case "RB": 	flx = "Elusiveness";
	        			break;
	        case "WR":	flx = "Catching";
		}
		buttonFlx = new Button(flx);
		panel.add(buttonFlx);
		
		frame.add("South", panel);
		
		frame.addWindowListener(new CloseListener());
		frame.setSize(350, 100);
		frame.setLocation(100, verticalPosition);
		frame.setVisible(true);
	}

	public void update(Observable obs, Object obj) {}

	public void addController(ActionListener controller) {
		buttonHt.addActionListener(controller); 
		buttonSpd.addActionListener(controller); 
		buttonStr.addActionListener(controller); 
		buttonFlx.addActionListener(controller); 
	}
	
	public Frame getFrame() {
		return frame;
	}

	public static class CloseListener extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			e.getWindow().setVisible(false);
			System.exit(0);
		} 
	} 

} 
