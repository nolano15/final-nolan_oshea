package mvc;

import java.util.Observable;

//Model doesn't know about View or Controller

public class Model extends Observable {
	private int ht, spd, str, flx;
	
	public Model() {
		ht = 85;
		spd = 85;
		str = 85;
		flx = 85;
	}

	public int getHt() {
		return ht;
	}

	public void setHt(int ht) {
		this.ht = ht;
		doNotify();
	}

	public int getSpd() {
		return spd;
	}

	public void setSpd(int spd) {
		this.spd = spd;
		doNotify();
	}

	public int getStr() {
		return str;
	}

	public void setStr(int str) {
		this.str = str;
		doNotify();
	}

	public int getFlx() {
		return flx;
	}

	public void setFlx(int flx) {
		this.flx = flx;
		doNotify();
	}

	private void doNotify() {
		setChanged();
		notifyObservers();
	}
}