package tools;

import players.Player;
import team.Team;

public abstract class PlayerFactory {
	protected abstract Player flexStat(String position, int flex);
	
	final public Player createPlayer(Team team, String position, int flex, int height, int speed, int strength) {
		// fluid interface
		// initializes yards to zero
		return flexStat(position, flex).setTeam(team).setHeight(height).setSpeed(speed).setStrength(strength).setYards(0);
	}
}
