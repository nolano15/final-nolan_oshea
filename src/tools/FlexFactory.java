package tools;

import players.Player;
import players.QB;
import players.RB;
import players.WR;

public class FlexFactory extends PlayerFactory {
	protected Player flexStat(String position, int flex) {
		Player player = null;
		switch(position) {
	        case "QB": 	player = new QB();
	        			break;
	        case "RB": 	player = new RB();
	        			break;
	        case "WR":	player = new WR();
		}
		return player.setFlex(flex);
	}
}
