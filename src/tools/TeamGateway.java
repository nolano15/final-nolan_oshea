package tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import team.Coach;
import team.Defense;
import team.Team;

public class TeamGateway {
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver",
								DB_CONNECTION = "jdbc:derby:Final;",
								DB_USER = "",
								DB_PASSWORD = "";
	public final static Logger LOGGER = Logger.getLogger(TeamGateway.class.getName());
	public Team save(Team team) {
		Connection dbConnection = null;
		PreparedStatement statement = null, statement2 = null;
		ResultSet rs;
		String sql;
		int coachID = 0, teamID = 0;
		try {
			dbConnection = getDBConnection();
			
			sql = "INSERT INTO Coaches(boost, dRating) VALUES(?, ?)";
			statement = dbConnection.prepareStatement(sql);
			statement.setString(1, team.getCoach().getBoost());
			statement.setInt(2, team.getD().getDRating());
			// execute insert SQL statement
			statement.executeUpdate();
			statement.close();
			
			// need coachID
			sql = "SELECT coachID FROM Coaches WHERE boost = ? AND dRating = ?";
			statement = dbConnection.prepareStatement(sql);
			statement.setString(1, team.getCoach().getBoost());
			statement.setInt(2, team.getD().getDRating());
			rs = statement.executeQuery();
			
			if(rs.next())
				coachID = rs.getInt("coachID");
			
			sql = "INSERT INTO Teams(coachID) VALUES(?)";
			statement2 = dbConnection.prepareStatement(sql);
			statement2.setInt(1, coachID);
			// execute insert SQL statement
			statement2.executeUpdate();
			statement2.close();
			
			// need teamID
			sql = "SELECT teamID FROM Teams WHERE coachID = ?";
			statement2 = dbConnection.prepareStatement(sql);
			statement2.setInt(1, coachID);
			rs = statement2.executeQuery();
			statement.close();
			
			sql = "INSERT INTO Players(teamID, position, ht, spd, str, flx) VALUES(?, ?, ?, ?, ?, ?)";
			if(rs.next())
				teamID = rs.getInt("teamID");
			
			// QB
			statement = dbConnection.prepareStatement(sql);
			statement.setInt(1, teamID);
			statement.setString(2, "QB");
			statement.setInt(3, team.getQb().getHeight());
			statement.setInt(4, team.getQb().getSpeed());
			statement.setInt(5, team.getQb().getStrength());
			statement.setInt(6, team.getQb().getFlex());
			// execute insert SQL statement
			statement.executeUpdate();
			statement.close();
			
			// RB
			statement = dbConnection.prepareStatement(sql);
			statement.setInt(1, teamID);
			statement.setString(2, "RB");
			statement.setInt(3, team.getRb().getHeight());
			statement.setInt(4, team.getRb().getSpeed());
			statement.setInt(5, team.getRb().getStrength());
			statement.setInt(6, team.getRb().getFlex());
			// execute insert SQL statement
			statement.executeUpdate();
			statement.close();
			
			// WR
			statement = dbConnection.prepareStatement(sql);
			statement.setInt(1, teamID);
			statement.setString(2, "WR");
			statement.setInt(3, team.getWr().getHeight());
			statement.setInt(4, team.getWr().getSpeed());
			statement.setInt(5, team.getWr().getStrength());
			statement.setInt(6, team.getWr().getFlex());
			// execute insert SQL statement
			statement.executeUpdate();
			
			// finally
			if (statement != null)
				statement.close();
			if (statement2 != null)
				statement2.close();
			if (dbConnection != null)
				dbConnection.close();
		} catch(SQLException e) {
			LOGGER.severe("Exception: "+e.getMessage());
		}
		return team;
	}
	
	public List<Team> findAll() {
		Connection dbConnection = null;
		PreparedStatement statement = null, statement2 = null;
		String sql;
		ResultSet rs, rs2;
		List<Team> teams = null;
		PlayerFactory pfact = new FlexFactory();
		Team team = null;
		Coach coach = null;
		Defense d = null;
		try {
			dbConnection = getDBConnection();
			
			sql = "SELECT * FROM Teams";
			statement = dbConnection.prepareStatement(sql);
			// execute select SQL statement
			rs = statement.executeQuery();
			
			// loop through every tuple
			for(;rs.next();) {
				teams = new ArrayList<Team>();
				sql = "SELECT * FROM Coaches WHERE coachID = ?";
				statement2 = dbConnection.prepareStatement(sql);
				statement2.setInt(1, rs.getInt("coachID"));
				rs2 = statement2.executeQuery();
				
				if(rs2.next()) {
					coach = new Coach(rs2.getString("boost"));
					d = new Defense(coach, rs2.getInt("dRating"));
				}
				statement2.close();
				
				sql = "SELECT * FROM Players WHERE teamID = ?";
				statement2 = dbConnection.prepareStatement(sql);
				statement2.setInt(1, rs.getInt("teamID"));
				rs2 = statement2.executeQuery();
				
				team = (new Team()).setCoach(coach).setD(d);
				for(;rs2.next();) {
					switch(rs2.getString("position")) {
			            case "QB": 	team.setQb(pfact.createPlayer(team, "QB", rs2.getInt("flx"), rs2.getInt("ht"), rs2.getInt("spd"), rs2.getInt("str")));
			            			break;
			            case "RB": 	team.setRb(pfact.createPlayer(team, "RB", rs2.getInt("flx"), rs2.getInt("ht"), rs2.getInt("spd"), rs2.getInt("str")));
    								break;
			            case "WR":	team.setWr(pfact.createPlayer(team, "WR", rs2.getInt("flx"), rs2.getInt("ht"), rs2.getInt("spd"), rs2.getInt("str")));
					}
				}
				statement2.close();
				teams.add(team);
			}
			// finally
			if (statement != null)
				statement.close();
			if (statement2 != null)
				statement2.close();
			if (dbConnection != null)
				dbConnection.close();
		} catch(SQLException e) {
			LOGGER.severe("Exception: "+e.getMessage());
		}
		return teams;
	}
	
	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch(ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch(SQLException e) {
			LOGGER.severe("Exception: "+e.getMessage());
		}
		return dbConnection;
	}
}