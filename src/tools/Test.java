package tools;

import team.Coach;
import team.Defense;
import team.Team;

class Test {

	@org.junit.jupiter.api.Test
	void testDB() {
		PlayerFactory pfact = new FlexFactory();
		Team team = new Team();
		Coach coach = new Coach("QB");
		team = team.setQb(pfact.createPlayer(team, "QB", 100, 72, 90, 80))
		.setRb(pfact.createPlayer(team, "RB", 100, 72, 90, 80))
		.setWr(pfact.createPlayer(team, "WR", 100, 72, 90, 80));
		team.setCoach(coach).setD(new Defense(coach, 90));
		TeamGateway db = new TeamGateway();
		db.save(team);
	}

}
