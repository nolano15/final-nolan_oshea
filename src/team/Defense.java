package team;
import players.Player;

public class Defense {
	private Coach coach;
	int dRating;

	public int engage(Player player) {
		return player.getRating(player.getTeam().getCoach()) - this.getDRating();
	}

	public int getDRating() {
		return dRating;//*getCoach().boost(this);
	}

	public void setDRating(int dRating) {
		this.dRating = dRating;
	}
	
	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public Defense(Coach coach, int dRating) {
		this.coach = coach;
		this.dRating = dRating;
	}
}
