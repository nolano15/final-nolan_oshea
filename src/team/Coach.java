package team;

import players.QB;
import players.RB;
import players.WR;

public class Coach {
	// playstyles
	// QB, RB, WR, D
	private String boost;
	
	// visits
	public int boost(QB qb) {
		return (int) (boost == "QB" ? 1.2 : 1);
	}
	
	public int boost(RB rb) {
		return (int) (boost == "RB" ? 1.2 : 1);
	}
	
	public int boost(WR wr) {
		return (int) (boost == "WR" ? 1.2 : 1);
	}
	
	/*int boost(Defense d) {
		return (int) (boost == "D" ? 1.2 : 1);
	}*/

	public String getBoost() {
		return boost;
	}

	public void setBoost(String boost) {
		this.boost = boost;
	}

	public Coach(String boost) {
		this.boost = boost;
	}
}
