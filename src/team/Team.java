package team;
import java.util.Random;

import players.Player;

public class Team {
	Coach coach;
	Defense d;
	Player qb, rb, wr;
	private int wins;

	// randomly pick run or pass
	public Player playcall() {
		return (new Random()).nextBoolean() ? getRb() : getWr();
	}

	public Coach getCoach() {
		return coach;
	}

	public Team setCoach(Coach coach) {
		this.coach = coach;
		return this;
	}

	public Defense getD() {
		return d;
	}

	public Team setD(Defense d) {
		this.d = d;
		return this;
	}

	public Player getQb() {
		return qb;
	}

	public Team setQb(Player qb) {
		this.qb = qb;
		return this;
	}

	public Player getRb() {
		return rb;
	}

	public Team setRb(Player rb) {
		this.rb = rb;
		return this;
	}

	public Player getWr() {
		return wr;
	}

	public Team setWr(Player wr) {
		this.wr = wr;
		return this;
	}

	public int getWins() {
		return wins;
	}

	public Team setWins(int wins) {
		this.wins = wins;
		return this;
	}
}
