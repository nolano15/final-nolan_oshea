package entry;
import players.Player;
import team.Team;

// purely business logic
public class Game {
	// returns winner
	// each team will have 3 drives
	public Team play(Team home, Team away) {
		int homeScore = 0, awayScore = 0;
		for(int i = 0; i < 3; i++)
			homeScore += drive(home, away);
		for(int i = 0; i < 3; i++)
			awayScore += drive(away, home);
		return homeScore > awayScore ? home : away;
	}
	
	// returns points scored
	private int drive(Team off, Team def) {
		int yards, downYards = 0, totalYards = 0, down = 1;
		Player player;
		while(totalYards < 100) {
			player = off.playcall();
			yards = def.getD().engage(player);
			player.setYards(player.getYards() + yards); // accumulate stats
			downYards += yards;
			totalYards += yards;
			if(downYards >= 10) {
				down = 1; // earned a first down
				downYards = 0;
			}
			else if(down == 3 && totalYards < 100) {
				if(totalYards > 60) // if in fg range
					return 3; // kick
			}
			else if(down == 4)
				return 0; // failed to score
			else
				down++;
		}
		return 7; // scored a touchdown
	}
}
