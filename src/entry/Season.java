package entry;

import java.util.List;

import mvc.FinalView;
import mvc.PlayerMVC;
import team.Coach;
import team.Defense;
import team.Team;
import tools.FlexFactory;
import tools.PlayerFactory;
import tools.TeamGateway;

// singleton/facade
public class Season {
	private static Season instance;
	
	static public Season instance() {
		if(instance == null) {
			return new Season();
		}
		return instance;
	}
	
	public static void main(String[] args) {
		Season season = instance();
		PlayerMVC qbMVC, rbMVC, wrMVC;
		Team team = new Team();
		Coach coach;
		PlayerFactory pfact = new FlexFactory();
		
		wrMVC = new PlayerMVC("WR");
		rbMVC = new PlayerMVC("RB");
	    qbMVC = new PlayerMVC("QB");
	    
	    while(wrMVC.getController().max > 0)
	    
		team.setQb(pfact.createPlayer(team, "QB", qbMVC.getModel().getFlx(), qbMVC.getModel().getHt(), qbMVC.getModel().getSpd(), qbMVC.getModel().getStr()))
				.setRb(pfact.createPlayer(team, "RB", rbMVC.getModel().getFlx(), rbMVC.getModel().getHt(), rbMVC.getModel().getSpd(), rbMVC.getModel().getStr()))
				.setWr(pfact.createPlayer(team, "WR", wrMVC.getModel().getFlx(), wrMVC.getModel().getHt(), wrMVC.getModel().getSpd(), wrMVC.getModel().getStr()));
		
		// coach will boost player with lowest strength
		if(team.getQb().getStrength() < team.getRb().getStrength() && team.getQb().getStrength() < team.getWr().getStrength())
			coach = new Coach("QB");
		else if(team.getRb().getStrength() < team.getQb().getStrength() && team.getRb().getStrength() < team.getWr().getStrength())
			coach = new Coach("RB");
		else
			coach = new Coach("WR");
		// defense default to 90 rating
		team = (new Team()).setCoach(coach).setD(new Defense(coach, 90)).setWins(0);
		season.sim(team);
		
		System.out.printf("WINS = %d\n", team.getWins());
		
		@SuppressWarnings("unused")
		FinalView end = new FinalView(team.getWins(), 50);
	}
	
	public Team sim(Team team) {
		TeamGateway db = new TeamGateway();
		Game game = new Game();
		int wins = 0;
		List<Team> teams = db.findAll();
		for(int i = 0; i < teams.size(); i++) {
			if(game.play(team, teams.get(i)) == team)
				wins++;
		}
		// save team forever
		return db.save(team.setWins(wins));
	}
}
