package players;

import team.Coach;

public class WR extends Player {
	int catching;
	
	public int getRating(Coach coach) {
		return (getHeight() + getSpeed() + getFlex())*coach.boost(this)/3 // avg stats
				+ (int) (.1*getTeam().getQb().getRating(coach)); // qb boost
    }

	public int getFlex() {
		return catching;
	}

	public WR setFlex(int catching) {
		this.catching = catching;
		return this;
	}
}
