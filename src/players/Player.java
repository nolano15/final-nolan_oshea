package players;

import team.Coach;
import team.Team;

public abstract class Player {
	private Team team;
	// stats will be 80-100
	int height, speed, strength;
	private int yards;
	
	// accept
	public abstract int getRating(Coach coach);
	
	public abstract int getFlex();
	public abstract Player setFlex(int flx);

	public int getHeight() {
		return height;
	}

	public Player setHeight(int height) {
		this.height = height;
		return this;
	}

	public int getSpeed() {
		return speed;
	}

	public Player setSpeed(int speed) {
		this.speed = speed;
		return this;
	}

	public int getStrength() {
		return strength;
	}

	public Player setStrength(int strength) {
		this.strength = strength;
		return this;
	}

	public Team getTeam() {
		return team;
	}

	public Player setTeam(Team team) {
		this.team = team;
		return this;
	}

	public int getYards() {
		return yards;
	}

	public Player setYards(int yards) {
		this.yards = yards;
		return this;
	}
}
