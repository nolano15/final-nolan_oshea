package players;

import team.Coach;

public class RB extends Player {
	int elusiveness;
	
	public int getRating(Coach coach) {
		return (getStrength() + getSpeed() + getFlex())*coach.boost(this)/3 // avg stats
				+ (int) (.1*getTeam().getQb().getRating(coach)); // slight qb boost
    }

	public int getFlex() {
		return elusiveness;
	}

	public RB setFlex(int elusiveness) {
		this.elusiveness = elusiveness;
		return this;
	}
}
