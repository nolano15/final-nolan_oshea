package players;

import team.Coach;

public class QB extends Player {
	int throwing;
	
	public int getRating(Coach coach) {
		return (getStrength() + getFlex())*coach.boost(this)/2;
    }
	
	public int getFlex() {
		return throwing;
	}

	public Player setFlex(int throwing) {
		this.throwing = throwing;
		return this;
	}
}
